﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TornadoMagic : MonoBehaviour
{
    public ParticleSystem Tornado;
    public ParticleSystem SkyFunnel;

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Alpha3))
        {
            Tornado.transform.position = gameObject.transform.position + gameObject.transform.forward * 10;
            Tornado.transform.rotation = Quaternion.Euler(new Vector3(-90f, gameObject.transform.rotation.eulerAngles.y, 0f));
            Tornado.Play();
            SkyFunnel.transform.position = new Vector3(Tornado.transform.position.x, 150f, Tornado.transform.position.z);
            SkyFunnel.Play();
        }
    }
}

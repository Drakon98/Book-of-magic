﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IceStrikeMagic : MonoBehaviour
{
    public ParticleSystem IceStrike;

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Alpha2))
        {
            var iceStrike = Instantiate(IceStrike);
            iceStrike.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y - 0.3f, gameObject.transform.position.z);
            iceStrike.transform.rotation = Quaternion.Euler(new Vector3(0f, gameObject.transform.rotation.eulerAngles.y, 0f));
            iceStrike.Play();
            iceStrike.GetComponent<ClearerParticipleSystem>().enabled = true;
        }
    }
}

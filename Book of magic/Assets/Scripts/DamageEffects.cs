﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageEffects : MonoBehaviour
{
    public int Damage;

    private void OnParticleCollision(GameObject other)
    {
        var creature = other.GetComponent<Creature>();
        creature.SetDamage(Damage);
    }
}

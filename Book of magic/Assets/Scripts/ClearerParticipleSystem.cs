﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClearerParticipleSystem : MonoBehaviour
{
    private ParticleSystem other;

    void Start()
    {
        other = gameObject.GetComponent<ParticleSystem>();
        Destroy(gameObject, other.main.startLifetime.constant);
    }

    
    void Update()
    {
        
    }
}

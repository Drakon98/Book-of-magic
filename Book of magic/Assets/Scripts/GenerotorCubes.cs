﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerotorCubes : MonoBehaviour
{
    public GameObject Cube;

    void Start()
    {
        for(int i = 0; i < 100; i++)
            Instantiate(Cube, new Vector3(i, i, i), Quaternion.Euler(new Vector3(Random.Range(0, 360), Random.Range(0, 360), Random.Range(0, 360))));
    }

}

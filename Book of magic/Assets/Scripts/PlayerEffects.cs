﻿using System;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Creature))]
public class PlayerEffects : MonoBehaviour
{
    public float StartSize;
    public Effect[] FireEffects;
    public Effect[] IceEffects;
    public Effect[] OtherEffects;
    public Effect[] SpecialEffects;
    private bool isPlayning;
    private bool isWait;

    private void OnParticleCollision(GameObject other)
    {
        if (other.gameObject.tag == "AttackMagic")
        {
            var creature = GetComponent<Creature>();
            if (other.gameObject.name == "Fireball(Clone)")
            {
                PlayEffects(FireEffects);
            }
            if (other.gameObject.name == "Ice strike(Clone)")
            {
                PlayEffects(IceEffects);
            }
            Destroy(other);
            if (creature.isDied && !isWait)
            {
                Debug.Log("isDied");
                isWait = true;
                PlayEffects(OtherEffects);
                PlayEffects(SpecialEffects, 5f);
                if(SpecialEffects.Length == 0)
                    Destroy(gameObject);
                else
                    Destroy(gameObject, 6f);
                if (isPlayning)
                    StopAllCoroutines();
            }
        }
    }

    private void PlayEffects(Effect[] effects)
    {
        for (int i = 0; i < effects.Length; i++)
        {
            effects[i].Play(gameObject, StartSize);
        }
    }

    private void PlayEffects(Effect[] effects, float startTime)
    {
        if(effects.Length > 0)
            StartCoroutine(WaitTime(effects, startTime));
    }

    private IEnumerator<WaitForSeconds> WaitTime(Effect[] effects, float startTime)
    {
        Debug.Log("Wait");
        yield return new WaitForSeconds(startTime);
        PlayEffects(effects);
        Debug.Log("Play");
        isPlayning = true;
    }
}

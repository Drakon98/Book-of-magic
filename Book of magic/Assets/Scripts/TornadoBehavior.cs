﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TornadoBehavior : MonoBehaviour
{
    private ParticleSystem tornado;
    private Vector3 target;
    
    void Start()
    {
        tornado = gameObject.GetComponent<ParticleSystem>();
        target = new Vector3(100f, 0f, 100f);
    }


    void Update()
    {
        Debug.Log(target);
        Debug.Log(tornado.gameObject.transform.position);
        Vector3.MoveTowards(tornado.gameObject.transform.position, target, 10f * Time.deltaTime);
    }
}

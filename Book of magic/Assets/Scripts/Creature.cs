﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Creature : MonoBehaviour
{
    [SerializeField]
    private int currentHealth;
    public int CurrentHealth { get => currentHealth; set { if (value >= 0) currentHealth = value; else currentHealth = 0; } }
    [SerializeField]
    private int maxHealth = 100;
    public int MaxHealth { get => maxHealth; set => maxHealth = value; }
    public bool isDied;

    private void Awake()
    {
        CurrentHealth = MaxHealth;
    }

    public void SetDamage(int damade)
    {
        CurrentHealth -= damade;
        if (CurrentHealth == 0)
            isDied = true;
    }  
}

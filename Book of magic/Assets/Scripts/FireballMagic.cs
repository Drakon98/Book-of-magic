﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballMagic : MonoBehaviour
{
    public ParticleSystem Fireball;

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Alpha1))
        {
            var fireball = Instantiate(Fireball);
            fireball.transform.position = gameObject.transform.position;
            fireball.transform.rotation = Quaternion.Euler(new Vector3(-80f, gameObject.transform.rotation.eulerAngles.y, -90f));
            fireball.Play();
            fireball.GetComponent<ClearerParticipleSystem>().enabled = true;
        }
    }
}

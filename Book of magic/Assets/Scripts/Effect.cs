﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EffectType { Ice, Fire, Other }

[CreateAssetMenu(fileName = "Effect", menuName = "Effect", order = 0)]
public class Effect : ScriptableObject
{
    public EffectType Type = EffectType.Ice;
    public ParticleSystem ParticleSystemEffect;

    public void Play(GameObject target, float startSize)
    {
        var participleSystemEffect = Instantiate(ParticleSystemEffect);
        participleSystemEffect.transform.position = target.transform.position;
        var mainparticipleSystemEffect = participleSystemEffect.main;
        //mainparticipleSystemEffect.startSize = participleSystemEffect.main.startSize.constantMin * startSize;
        //mainparticipleSystemEffect.startSize = participleSystemEffect.main.startSize.constantMax * startSize;
        participleSystemEffect.GetComponent<ClearerParticipleSystem>().enabled = true;
        participleSystemEffect.Play();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(Rigidbody), typeof(NavMeshAgent))]
public class EnemyController : MonoBehaviour
{
    public float Force;
    private Rigidbody rigidbodyEnemy;
    private NavMeshAgent agentEnemy;
    private GameObject player;
    private Transform enemy;
    private bool isPlayer;
    private bool isTarget;

    private void Start()
    {
        rigidbodyEnemy = GetComponent<Rigidbody>();
        enemy = GetComponent<Transform>();
        agentEnemy = GetComponent<NavMeshAgent>();
    }

    private void FixedUpdate()
    {
        if (isPlayer)
        {
            agentEnemy.enabled = true;
            agentEnemy.destination = player.transform.position;
        }
        else
        {
            agentEnemy.enabled = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            isPlayer = true;
            player = other.gameObject;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            isPlayer = true;
            player = other.gameObject;
            //Debug.Log(isPlayer);
            //rigidbodyEnemy.AddForceAtPosition(Vector3.back * Speed * Time.deltaTime, player.transform.position, ForceMode.VelocityChange);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            isPlayer = false;
            player = null;
            //Debug.Log(isPlayer);
        }
    }
}
